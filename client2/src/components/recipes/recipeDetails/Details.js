import React from 'react'

const Details = ({ selRecipe }) => {
    return (
        <div>
            <div>Preperation Time:{selRecipe.prepTime}</div>
            <div>Servings:{selRecipe.servings}</div>
        </div>
    )
}

export default Details
