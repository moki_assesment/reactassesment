import React from 'react'

import './../recipes.css'

const Directions = ({ selRecipe }) => {
    return (
        <>
            {
                selRecipe.directions.map((direction, index)=>{
                    return(
                        <>
                            <div className="direction-list">
                                <strong>Step {index+1}:</strong>
                                <div className="direction-text">
                                    - {direction.instructions}
                                </div>
                            </div>
                        </>
                    )
                })
            }  
        </>
    )
}

export default Directions
