import React from 'react'
import { Row, Col } from 'antd'

import './../recipes.css'

const Ingredients = ({ selRecipe, specials }) => {
    return (
        <>
            <Row>
                <Col span={12}><h3>Ingredients</h3></Col>
                <Col span={12}><h3>Specials</h3></Col>
            </Row>
            
            {
                selRecipe.ingredients.map((ingredient) => {
                    const { uuid, name, amount, measurement} = ingredient

                    const specialRes = specials.filter((special) => {
                        return special.ingredientId === uuid
                    })

                    return(
                        <div className="ingredient-list">
                            <div className="ing-name">{name}</div>
                            <Row>
                                {/* <Col span={2}></Col> */}
                                <Col span={12}>
                                    <ul>
                                        <li>Amount : {amount}</li>
                                        <li>Measurement : {measurement ===""?'None':measurement}</li>
                                    </ul>
                                </Col>
                                <Col span={12}>
                                    {
                                        specialRes.map((special) => {
                                            
                                            return(
                                                <>
                                                    <div>Title: {special.title}</div>
                                                    <div>Type: {special.type}</div>
                                                    <div>Text: {special.text}</div>
                                                </>
                                            )
                                            
                                        })
                                    }
                                </Col>
                                {/* <Col span={2}></Col> */}
                            </Row>
                        </div>
                    )
                })
            }
        </>
    )
}

export default Ingredients
