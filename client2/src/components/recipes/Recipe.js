import React, { useEffect, useState } from 'react'
import axios from 'axios'
import RecipeList from './RecipeList'
import RecipePreview from './RecipePreview'

// import './recipe.css'

import { Row, Col, Button } from 'antd';

const Recipe = () => {

    // I have used props insted of context to get the data from one source because the app is not that big though
    // not much state to be set and call

    const [recipes, setRecipes] = useState([])
    const [specials, setSpecials] = useState([])
    const [selRecipe, setSelRecipe] = useState([])

    useEffect(() => {
        axios.get('http://localhost:3001/recipes')
        .then((res) => {
            console.log(res)
            setRecipes(res.data)
        }).catch((err) => {
            console.log(err)
        })
        axios.get('http://localhost:3001/specials')
        .then((res) => {
            console.log(res)
            setSpecials(res.data)
        }).catch((err) => {
            console.log(err)
        })
    }, []) 

    return (
        <>
            <Row>
                <Col span={2}></Col>
                <Col span={20}>
                    <Button type="primary" size='small' className="mb-2">
                        Add New Recipe
                    </Button>
                </Col>
                <Col span={2}></Col>
            </Row>
            <Row>
                <Col span={2}></Col>
                <Col span={6} className="recipeList" style={{boxShadow:'rgb(134 134 134 / 28%) 0px 0px 15px 5px'}}>
                    <RecipeList 
                        recipes={recipes} 
                        setSelRecipe={setSelRecipe}
                    /> 
                </Col>
                <Col span={2}></Col>
                <Col span={12} className="recipePreview" style={{boxShadow:'rgb(134 134 134 / 28%) 0px 0px 15px 5px'}}>
                    <RecipePreview selRecipe={selRecipe} specials={specials} /> 
                </Col>
                <Col span={2}></Col>
            </Row>
        </>

    )
}

export default Recipe
