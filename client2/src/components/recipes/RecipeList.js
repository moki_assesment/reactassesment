import React from 'react'
import { Row, Col } from 'antd';
import './recipes.css'

const RecipeList = ( { recipes, setSelRecipe } ) => {
    return (
        <div>
            <Row>
                {
                    recipes.length <= 0 ? "No data" 
                    :
                        recipes.map((recipe) => {
                            return(
                                <Col span={12} key={recipe.uuid} onClick={() => {
                                        setSelRecipe(recipe)
                                    }}>
                                    <img 
                                        className='listImg'
                                        src={require(`./../../brand${recipe.images.medium}`).default} 
                                    />
                                    <div className="listTitle">{recipe.title}</div> 
                                </Col>
                            )
                        })
                }
                
            </Row>
        </div>
    )
}

export default RecipeList
