import React from 'react'
import Ingredients from './recipeDetails/Ingredients'


import { Tabs, Button, TimePicker } from 'antd'
import Directions from './recipeDetails/Directions';
import Details from './recipeDetails/Details';

const { TabPane } = Tabs;

const RecipePreview = ({ selRecipe, specials }) => {

    return (
        <div className='recipe-preview'>
            
            {
                selRecipe.length <= 0? 
                    'No Data'
                :
                    <>
                        <div className="recipe-preview">
                            <img src={require(`./../../brand${selRecipe.images.medium}`).default} />
                            <div className="recipe-name">
                                {selRecipe.description}
                            </div>
                        </div>
                        <div>
                            <Tabs defaultActiveKey="1" centered>
                                <TabPane tab="Details" key="1">
                                    <Details selRecipe={selRecipe}/>
                                </TabPane>
                                <TabPane tab="Ingredients" key="2">
                                    <Ingredients selRecipe={selRecipe} specials={specials} />
                                </TabPane>
                                <TabPane tab="Directions" key="3">
                                    <Directions selRecipe={selRecipe}/>
                                </TabPane>
                            </Tabs>
                        </div>
                    </>
            }
        </div>
    )
}

export default RecipePreview
