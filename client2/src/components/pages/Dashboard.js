import React from 'react'
import Recipe from '../recipes/Recipe'
import 'antd/dist/antd.css';


import { Layout, Menu } from 'antd';
const { Header, Content, Footer } = Layout;

const Dashboard = () => {

    return (
        <Layout style={{minHeight:'715px'}}>
            <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
                <div className="logo" />
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                <Menu.Item key="1">nav 1</Menu.Item>
                <Menu.Item key="2">nav 2</Menu.Item>
                <Menu.Item key="3">nav 3</Menu.Item>
                </Menu>
            </Header>
            <Content className="site-layout" style={{ padding: '0 50px', marginTop: 64 }}>
                <div className="site-layout-background" style={{ padding: 24, minHeight: 380 }}>
                    <Recipe />
                </div>
            </Content>
            <Footer style={{ textAlign: 'center', background:'#e0e0e0' }}>Cook Book App ©2021</Footer>
        </Layout>
    )
}

export default Dashboard
